const { createApolloFetch } = require('apollo-fetch');
const { get_users, get_users_by_id ,get_activity_by_userid ,get_activity_by_id } = require('./query');
const { insert_users,insert_activity } = require('./mutation');
const fetch = createApolloFetch({
    uri: 'https://vartikaapp.herokuapp.com/v1/graphql',
});

console.log('the user mutation is', insert_users);
module.exports = {
    Query: {
        get_users: () => {
            return fetch({
                query: get_users
            }).then(res => {
                console.log(res.data);
                return (res.data.user_user1);
            })
        },
        get_user_by_id: (_, args) => {
            return fetch({
                query: get_users_by_id,
                variables: { userId: args.id }
            }).then((resp) => {
                console.log('response in user id fetch', resp.data.user_user1[0]);
                return resp.data.user_user1[0];
            })
        },
        get_activity_by_user_id: (_, args) => {
            return fetch({
                query: get_activity_by_userid,
                variables: { userId: args.id }
            }).then((resp) => {
                console.log('response in user id fetch', resp.data.user_activity_UserActivity);
                return resp.data.user_activity_UserActivity;
            })

        },
        get_activity_by_id: (_, args) => {
            return fetch({
                query: get_activity_by_id,
                variables: { id: args.id }
            }).then((resp) => {
                console.log('response in user id fetch', resp.data.user_activity_UserActivity[0]);
                return resp.data.user_activity_UserActivity[0];
            })

        },
    },

    Mutation: {
        insert_user: (_, args) => {
            console.log('the arguments are', args);
            return fetch({
                query: insert_users,
                variables: { name: args.name, userContactNumber: args.mobile, email: args.email }
            }).then((resp) => {
                console.log(resp.data.insert_user_user1.returning[0]);
                return resp.data.insert_user_user1.returning[0];
            },(error) => {
                console.log('the error is', error);
            })

        },
        insert_user_activity: (_, args) => {
            return fetch({
                query: insert_activity,
                variables: { userId: args.userId, event: args.event, description: args.description }
            }).then((resp) => {
                console.log(resp.data.insert_user_activity_UserActivity);
                return resp.data.insert_user_activity_UserActivity;
            },(error) => {
                console.log('the error is', error);
            })

        },
    }

}
