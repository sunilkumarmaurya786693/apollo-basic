
const get_users = `
query {
    user_user1{
        userName
        userId
        userContactNumber
        userEmail
    }
}
`;

const get_users_by_id = `
query getUserByUserId($userId: uuid!) {
    user_user1(where: {userId: {_eq: $userId}}) {
      userName
      userId
      userEmail
      userContactNumber
    }
  }
  `
  const get_activity_by_userid = `
query getActivityByUserId($userId: uuid!) {
    user_activity_UserActivity(where: {userId: {_eq: $userId}}) {
      event
      created_at
      description
      updated_at
    }
  }
  
`
const get_activity_by_id = `
query getActivityById($id: uuid!) {
    user_activity_UserActivity(where: {id: {_eq: $id}}) {
      event
      created_at
      description
      updated_at
    }
  }
  
`



module.exports = {
    get_users,
    get_users_by_id,
    get_activity_by_userid,
    get_activity_by_id
}