const insert_users = `
mutation($name:String!,$userContactNumber:String!,$email:String!){
  insert_user_user1(objects:{userName:$name,userContactNumber:$userContactNumber,userEmail:$email})
  {
  returning{
    userId
    userName
    userEmail
    userContactNumber
  }
  affected_rows
}
}
`;
const insert_activity = `
mutation ($userId: uuid!, $event: String!, $description: String!) {
  insert_user_activity_UserActivity(objects: {userId: $userId, event: $event, description: $description}) {
    returning {
      userId
      event
      description
    }
    affected_rows
  }
}
`;


module.exports = {
  insert_users,insert_activity
}